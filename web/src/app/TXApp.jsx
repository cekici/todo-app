/* eslint-disable */
/* react/no-multi-comp*/

import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import ROUTE_DASHBOARD from '../constants/routePaths';
import ConnectedSmartTXDashboardPageContentWithRouter from '../pages/dashboard/TXDashboardPageContent';

import './_tx-app.scss';

class SmartTXApp extends Component {
  renderPageRoutes = () => {
    return <Switch>
      <Route
        path={ROUTE_DASHBOARD}
        component={ConnectedSmartTXDashboardPageContentWithRouter}
      />

      <Redirect
        from="/*"
        to="/dashboard"
      />
    </Switch>;
  }

  render() {

    return (
      <div className="tx-app-container">
        {this.renderPageRoutes()}
      </div>
    );
  }
}

const mapStateToProps = (state = {}) => {
  const {
    todoState: {
      todos
    }
  } = state;

  return {
    todos
  };
};

const ConnectedSmartTXApp = connect(mapStateToProps)(SmartTXApp);
const ConnectedSmartTXAppWithRouter = withRouter(ConnectedSmartTXApp);

class RootConnectedSmartTXApp extends Component {
  render() {
    return (
      <Router basename="/">
        <Route component={ConnectedSmartTXAppWithRouter}/>
      </Router>
    );
  }
}

/* eslint-enable */

export default RootConnectedSmartTXApp;
