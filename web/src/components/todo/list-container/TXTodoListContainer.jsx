import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Spinner } from 'travix-ui-kit';
import TXTodoItem from '../item/TXTodoItem';

import './_tx-todo-list-container.scss';

export default class TXTodoListContainer extends Component {
  static propTypes = {
    activeDeleteTodoActionCandidateId: PropTypes.string,
    hasActiveDeleteTodoAction: PropTypes.bool.isRequired,
    hasActiveGetTodosAction: PropTypes.bool.isRequired,
    onTodoDeleteClick: PropTypes.func.isRequired,
    onTodoEditClick: PropTypes.func.isRequired,
    onToggleTodoCompletedClick: PropTypes.func.isRequired,
    todos: PropTypes.array.isRequired,
  }

  renderTodosList = () => {
    const {
      todos,
      hasActiveDeleteTodoAction,
      activeDeleteTodoActionCandidateId,
      onTodoEditClick,
      onTodoDeleteClick,
      onToggleTodoCompletedClick,
    } = this.props;

    return (todos.length
      ? <ul className="tx-todo-list">
        {todos.map(todo =>
          (<TXTodoItem
            activeDeleteTodoActionCandidateId={activeDeleteTodoActionCandidateId}
            hasActiveDeleteTodoAction={hasActiveDeleteTodoAction}
            key={todo.id}
            onTodoDeleteClick={onTodoDeleteClick}
            onTodoEditClick={onTodoEditClick}
            onToggleTodoCompletedClick={onToggleTodoCompletedClick}
            todo={todo}
          />
          ))}
      </ul>
      : <p className="tx-todo-list-empty-text">No todos yet</p>
    );
  }

  render() {
    const {
      hasActiveGetTodosAction,
    } = this.props;

    return (
      <div className="tx-todo-list-container">
        {hasActiveGetTodosAction
          ? <Spinner size="s" />
          : this.renderTodosList()
        }
      </div>
    );
  }
}
