import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button, Checkbox, Spinner, Collapse, CollapseItem } from 'travix-ui-kit';

import './_tx-todo-item.scss';

export default class TXTodoItem extends Component {
  static propTypes = {
    activeDeleteTodoActionCandidateId: PropTypes.string,
    hasActiveDeleteTodoAction: PropTypes.bool.isRequired,
    onTodoDeleteClick: PropTypes.func.isRequired,
    onTodoEditClick: PropTypes.func.isRequired,
    onToggleTodoCompletedClick: PropTypes.func.isRequired,
    todo: PropTypes.object.isRequired,
  }

  handleEditClick = () => {
    const { todo, onTodoEditClick } = this.props;
    onTodoEditClick(todo);
  }

  handleToggleTodoCompletedClick = (e) => {
    const { todo, onToggleTodoCompletedClick } = this.props;

    e.preventDefault();

    onToggleTodoCompletedClick(todo);
  }

  handleDeleteClick = () => {
    const { todo, onTodoDeleteClick } = this.props;

    onTodoDeleteClick(todo);
  }

  render() {
    const {
      todo,
      hasActiveDeleteTodoAction,
      activeDeleteTodoActionCandidateId,
    } = this.props;

    const {
      id,
      title,
      description,
      isCompleted,
    } = todo;

    return (
      <li
        className="tx-todo-item"
        onClick={this.handleClick}
      >

        <Checkbox
          checked={isCompleted}
          className="tx-todo-item-checkbox"
          name={`txChecbox-${id}`}
          onChange={this.handleToggleTodoCompletedClick}
        />

        <Collapse>
          <CollapseItem
            id={id}
            title={
              <div className="tx-todo-item-collapse-title">
                <h1 className="tx-todo-item-title">{title}</h1>
              </div>}
          >
            <p className="tx-todo-item-description">{description}</p>

            <Button
              className="tx-todo-item-edit-todo-button"
              onClick={this.handleEditClick}
              size="s"
              type="button"
            >
              {'Edit'}
            </Button>

            <Button
              className="tx-todo-item-delete-todo-button"
              disabled={hasActiveDeleteTodoAction}
              onClick={this.handleDeleteClick}
              size="s"
              type="button"
            >
              {hasActiveDeleteTodoAction && activeDeleteTodoActionCandidateId === id
                ? <Spinner size="s" />
                : "Delete"
              }
            </Button>
          </CollapseItem>
        </Collapse>

      </li>
    );
  }
}
