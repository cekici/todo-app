import React from 'react';
import {shallow} from 'enzyme';

import TXTodoItem from './TXTodoItem';

const setup = (id = 1,
                 title = "Mock Title",
                 description = "Mock Description",
                 isCompleted = false) => {

  const callbacks = {
    onTodoDeleteClick: jest.fn(),
    onTodoEditClick: jest.fn(),
    onToggleTodoCompletedClick: jest.fn()
  };

  const todo = {
    id: `mock-todo-${id}`,
    title,
    description,
    isCompleted
  };

  const hasActiveDeleteTodoAction = false;
  const activeDeleteTodoActionCandidateId = "";
  const wrapper = shallow(
    <TXTodoItem activeDeleteTodoActionCandidateId={activeDeleteTodoActionCandidateId}
                hasActiveDeleteTodoAction={hasActiveDeleteTodoAction}
                {
                  ...callbacks
                }
                todo={todo}/>
  );

  return {
    wrapper,
    todo,
    callbacks
  }
};

describe('TXTodoItem', () => {
  it('should render title', () => {
    const mockTitle = "My Title";
    const {wrapper} = setup(1, mockTitle);
    const title = wrapper.find('.tx-todo-item-title');
    expect(title.text()).toMatch(mockTitle);
  });

  it('should render description', () => {
    const mockTitle = "My Title";
    const mockDesc = "Mock Description";
    const {wrapper} = setup(1, mockTitle, mockDesc);
    const description = wrapper.find('.tx-todo-item-description');
    expect(description.text()).toMatch(mockDesc);
  });

  it('should call onTodoEditClick when edit button is clicked', () => {
    const {wrapper, callbacks} = setup();
    const button = wrapper.find('.tx-todo-item-edit-todo-button');
    button.simulate('click');
    expect(callbacks.onTodoEditClick).toBeCalled();
  });

  it('should call onTodoDeleteClick when delete button is clicked', () => {
    const {wrapper, callbacks} = setup();
    const button = wrapper.find('.tx-todo-item-delete-todo-button');
    button.simulate('click');
    expect(callbacks.onTodoDeleteClick).toBeCalled();
  });

  it('should call onToggleTodoCompletedClick when checkbox is changed', () => {
    const {wrapper, callbacks} = setup();
    const checkbox = wrapper.find('.tx-todo-item-checkbox');
    checkbox.simulate('click');
    expect(callbacks.onToggleTodoCompletedClick).toBeCalled();
  });
});
