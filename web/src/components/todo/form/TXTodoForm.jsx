import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button, Input, Spinner } from 'travix-ui-kit';

import './_tx-todo-form.scss';

export default class TXTodoForm extends Component {
  static propTypes = {
    errors: PropTypes.object.isRequired,
    hasActiveFormSubmitAction: PropTypes.bool.isRequired,
    initialValues: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      shouldDisplayErrors: false,
      values: {
        ...props.initialValues,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors !== this.props.errors) {
      this.setState({
        shouldDisplayErrors: true,
      });
    }
  }

  handleTitleChange = (event) => {
    const { values } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      values: {
        ...values,
        title: event.target.value,
      },
    });
  }

  handleDescriptionChange = (event) => {
    const { values } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      values: {
        ...values,
        description: event.target.value,
      },
    });
  }

  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { values } = this.state;
    const validation = {};

    event.preventDefault();

    if (!values.title) {
      validation.title = {
        message: "Title can't be empty",
      };
    }

    if (!values.description) {
      validation.description = {
        message: "Description can't be empty",
      };
    }

    onSubmit(values, validation);
  }

  render() {
    const { errors, hasActiveFormSubmitAction } = this.props;
    const { values, shouldDisplayErrors } = this.state;

    const errorArray = Object.keys(errors).map((key) => {
      return {
        key: key,
        value: errors[key].message,
      };
    });

    return (
      <form
        className="tx-todo-form"
        onSubmit={this.handleSubmit}
      >

        <div className="tx-todo-form-fields">
          <div className="tx-todo-form-field">
            <label
              className="tx-todo-form-input-label"
              htmlFor="txTodoFormTitle"
            >
              Title
            </label>
            <Input
              className="tx-todo-form-input"
              name="txTodoFormTitle"
              onChange={this.handleTitleChange}
              status={shouldDisplayErrors && errors.title ? "error" : "valid"}
              value={values.title}
            />
          </div>

          <div className="tx-todo-form-field">
            <label
              className="tx-todo-form-input-label"
              htmlFor="txTodoFormDescription"
            >
              Description
            </label>
            <Input
              className="tx-todo-form-textarea"
              multiline
              name="txTodoFormDescription"
              onChange={this.handleDescriptionChange}
              status={shouldDisplayErrors && errors.description ? "error" : "valid"}
              value={values.description}
            />
          </div>
        </div>

        {errorArray.length > 0
          ? <ul className="tx-todo-form-error-list">
            {errorArray.map(error =>
              (
                <li
                  className="tx-todo-form-error-item"
                  key={error.key}
                >
                  {error.value}
                </li>
              ),
            )}
          </ul> : null
        }

        <Button
          className="tx-todo-form-submit-button"
          size="s"
          type="submit"
        >
          {hasActiveFormSubmitAction
            ? <Spinner size="s" />
            : 'Submit'
          }
        </Button>
      </form>);
  }
}
