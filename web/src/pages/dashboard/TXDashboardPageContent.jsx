import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { Button, Modal, Tabs, Tab } from 'travix-ui-kit';

import * as todoActions from '../../core/todo/todoActions';
import TXTodoListContainer from '../../components/todo/list-container/TXTodoListContainer';
import TXTodoForm from '../../components/todo/form/TXTodoForm';

import './_tx-dashboard-page-content.scss';

const TAB_VALUES = {
  ALL: "all",
  COMPLETED: "completed",
  UNCOMPLETED: "uncompleted",
};

class SmartTXDashboardPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalProps: {
        isTodoFormModalVisible: false,
        headerText: '',
      },
      tab: TAB_VALUES.ALL,
      initialFormValues: {},
      errors: {},
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(todoActions.getTodos());
  }

  componentWillReceiveProps(nextProps) {
    const {
      todoApiErrors,
      hasActiveCreateTodoAction,
      hasActiveUpdateTodoAction,
    } = this.props;

    if (!nextProps.todoApiErrors !== todoApiErrors) {
      this.setState({
        errors: nextProps.todoApiErrors,
      });
    }

    if (isEmpty(nextProps.todoApiErrors) &&
      ((!nextProps.hasActiveCreateTodoAction && hasActiveCreateTodoAction) ||
        (!nextProps.hasActiveUpdateTodoAction && hasActiveUpdateTodoAction))) {
      const { modalProps } = this.state;
      const { isTodoFormModalVisible } = modalProps;

      if (isTodoFormModalVisible) {
        this.setState({
          modalProps: {
            ...modalProps,
            isTodoFormModalVisible: !isTodoFormModalVisible,
          },
        });
      }
    }
  }

  handleCreateTodoButtonClick = () => {
    const { modalProps } = this.state;

    this.setState({
      initialFormValues: {
        title: '',
        description: '',
      },
      modalProps: {
        ...modalProps,
        headerText: 'Create a New Todo',
        isTodoFormModalVisible: true,
      },
    });
  }

  handleTodoEditClick = (todo) => {
    const { modalProps } = this.state;

    this.setState({
      initialFormValues: {
        ...todo,
      },
      modalProps: {
        ...modalProps,
        headerText: 'Update Todo',
        isTodoFormModalVisible: true,
      },
    });
  }

  handleTodoDeleteClick = (todo) => {
    const { dispatch } = this.props;

    dispatch(todoActions.deleteTodo(todo.id));
  }

  handleToggleTodoCompletedClick = (todo) => {
    const { dispatch } = this.props;

    dispatch(todoActions.updateTodo({
      ...todo,
      isCompleted: !todo.isCompleted,
    }));
  }

  handleTodoFormModalClose = () => {
    const { modalProps } = this.state;

    this.setState({
      modalProps: {
        ...modalProps,
        isTodoFormModalVisible: false,
      },
    });
  }

  handleTabClick = (tab) => {
    this.setState({
      tab,
    });
  }

  handleTodoFormSubmit = (todo, validation) => {
    const { dispatch } = this.props;

    if (!validation.title && !validation.description) {
      if (todo.id) {
        dispatch(todoActions.updateTodo(todo));
      } else {
        dispatch(todoActions.createTodo(todo));
      }
    } else {
      this.setState({
        errors: validation,
      });
    }
  }

  render() {
    const {
      todos,
      hasActiveGetTodosAction,
      hasActiveCreateTodoAction,
      hasActiveUpdateTodoAction,
      hasActiveDeleteTodoAction,
      activeDeleteTodoActionCandidateId,
    } = this.props;
    const {
      initialFormValues,
      errors,
      tab,
      modalProps,
    } = this.state;

    let filteredTodos;

    if (tab === TAB_VALUES.ALL) {
      filteredTodos = todos;
    } else if (tab === TAB_VALUES.COMPLETED) {
      filteredTodos = todos.filter(todo => todo.isCompleted);
    } else if (tab === TAB_VALUES.UNCOMPLETED) {
      filteredTodos = todos.filter(todo => !todo.isCompleted);
    }

    return (
      <div className="tx-dashboard-page">
        <h1 className="tx-dashboard-welcome-title">{`Welcome to Todo App`}</h1>

        <Button
          className="tx-dashboard-page-create-todo-button"
          onClick={this.handleCreateTodoButtonClick}
          size="s"
          type="button"
        >
          {'Create New Todo'}
        </Button>

        <Tabs
          activeTab={tab}
          className="tx-dashboard-page-todo-list-tabs"
          name="txDashboardTab"
          onChange={this.handleTabClick}
        >
          <Tab title="All" value={TAB_VALUES.ALL} />
          <Tab title="Completed" value={TAB_VALUES.COMPLETED} />
          <Tab title="Uncompleted" value={TAB_VALUES.UNCOMPLETED} />
        </Tabs>

        <TXTodoListContainer
          activeDeleteTodoActionCandidateId={activeDeleteTodoActionCandidateId}
          hasActiveDeleteTodoAction={hasActiveDeleteTodoAction}
          hasActiveGetTodosAction={hasActiveGetTodosAction}
          onTodoDeleteClick={this.handleTodoDeleteClick}
          onTodoEditClick={this.handleTodoEditClick}
          onToggleTodoCompletedClick={this.handleToggleTodoCompletedClick}
          todos={filteredTodos}
        />

        <Modal
          active={modalProps.isTodoFormModalVisible}
          onClose={this.handleTodoFormModalClose}
          overlay
          title={(
            <header className="tx-todo-form-modal-header">
              <h1>{modalProps.headerText}</h1>
            </header>
          )}
        >
          <TXTodoForm
            errors={errors}
            hasActiveFormSubmitAction={hasActiveCreateTodoAction || hasActiveUpdateTodoAction}
            initialValues={initialFormValues}
            onSubmit={this.handleTodoFormSubmit}
          />
        </Modal>
      </div>);
  }
}

function mapStateToProps(state) {
  const {
    todoState: {
      todos,
      todoApiErrors,
      hasActiveGetTodosAction,
      hasActiveCreateTodoAction,
      hasActiveUpdateTodoAction,
      hasActiveDeleteTodoAction,
      activeDeleteTodoActionCandidateId,
    },
  } = state;

  return {
    todos,
    todoApiErrors,
    hasActiveGetTodosAction,
    hasActiveCreateTodoAction,
    hasActiveUpdateTodoAction,
    hasActiveDeleteTodoAction,
    activeDeleteTodoActionCandidateId,
  };
}

const ConnectedSmartTXDashboardPageContent = connect(mapStateToProps)(SmartTXDashboardPageContent);
const ConnectedSmartTXDashboardPageContentWithRouter = withRouter(ConnectedSmartTXDashboardPageContent);

export default ConnectedSmartTXDashboardPageContentWithRouter;
