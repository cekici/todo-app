const FULFILLED_KEYWORD = 'FULFILLED';
const REJECTED_KEYWORD = 'REJECTED';

function createActions(actionName) {
  return {
    base: actionName,
    get fulfilled() {
      return `${this.base}_${FULFILLED_KEYWORD}`;
    },
    get rejected() {
      return `${this.base}_${REJECTED_KEYWORD}`;
    },
  };
}

export const GET_TODOS = createActions('GET_TODOS');
export const CREATE_TODO = createActions('CREATE_TODO');
export const UPDATE_TODO = createActions('UPDATE_TODO');
export const DELETE_TODO = createActions('DELETE_TODO');
