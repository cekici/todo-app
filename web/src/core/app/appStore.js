import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import todoSagas from '../todo/todoSagas';
import TodoReducer from '../todo/TodoReducer';

function* rootSaga() {
  yield all([
    ...todoSagas,
  ]);
}

const reducers = {
  todoState: new TodoReducer(),
};

const rootReducer = combineReducers(reducers);
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);

export default store;

