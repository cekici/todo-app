import networkManager from '../../common/network/networkManager';

const todoApi = {
  getTodos() {
    return networkManager.api.get('/tasks');
  },

  createTodo(todo) {
    return networkManager.api.post('/task', todo);
  },

  updateTodo(todo) {
    const { id, title, description, isCompleted } = todo;

    return networkManager.api.put(`/task/${id}`, {
      title,
      description,
      isCompleted,
    });
  },

  deleteTodo(id) {
    return networkManager.api.delete(`/task/${id}`);
  },
};

export default todoApi;
