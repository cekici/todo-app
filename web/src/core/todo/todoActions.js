import * as ACTIONS from '../../constants/actionTypes';

export const getTodos = () => ({
  type: ACTIONS.GET_TODOS.base,
});

export const getTodosFulfilled = response => ({
  type: ACTIONS.GET_TODOS.fulfilled,
  payload: response,
});

export const getTodosRejected = error => ({
  type: ACTIONS.GET_TODOS.rejected,
  payload: error,
  isError: true,
});

export const createTodo = todo => ({
  type: ACTIONS.CREATE_TODO.base,
  payload: {
    todo,
  },
});

export const createTodoFulfilled = response => ({
  type: ACTIONS.CREATE_TODO.fulfilled,
  payload: {
    response,
  },
});

export const createTodoRejected = error => ({
  type: ACTIONS.CREATE_TODO.rejected,
  payload: error,
  isError: true,
});

export const updateTodo = todo => ({
  type: ACTIONS.UPDATE_TODO.base,
  payload: {
    todo,
  },
});

export const updateTodoFulfilled = response => ({
  type: ACTIONS.UPDATE_TODO.fulfilled,
  payload: {
    response,
  },
});

export const updateTodoRejected = error => ({
  type: ACTIONS.UPDATE_TODO.rejected,
  payload: error,
  isError: true,
});

export const deleteTodo = id => ({
  type: ACTIONS.DELETE_TODO.base,
  payload: {
    id,
  },
});

export const deleteTodoFulfilled = response => ({
  type: ACTIONS.DELETE_TODO.fulfilled,
  payload: response,
});

export const deleteTodoRejected = error => ({
  type: ACTIONS.DELETE_TODO.rejected,
  payload: error,
  isError: true,
});
