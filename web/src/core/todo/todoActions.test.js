import * as ACTIONS from '../../constants/actionTypes.js';
import * as todoActions from './todoActions.js';

describe('actions', () => {
  it('should create an action to get todos', () => {
    const expectedAction = {
      type: ACTIONS.GET_TODOS.base,
    };
    expect(todoActions.getTodos()).toEqual(expectedAction);
  });
});
