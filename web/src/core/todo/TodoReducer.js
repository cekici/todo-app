import * as ACTIONS from '../../constants/actionTypes';

const defaultInitialState = {
  todos: [],
  todoApiErrors: {},
  hasActiveGetTodosAction: false,
  hasActiveCreateTodoAction: false,
  hasActiveUpdateTodoAction: false,
  hasActiveDeleteTodoAction: false,
  activeDeleteTodoActionCandidateId: "",
};

function TodoReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.GET_TODOS.base: {
        newState = {
          ...state,
          hasActiveGetTodosAction: true,
        };
        break;
      }


      case ACTIONS.GET_TODOS.fulfilled: {
        const { tasks: todos } = action.payload.data;

        newState = {
          ...state,
          todos,
          hasActiveGetTodosAction: false,
        };

        break;
      }

      case ACTIONS.GET_TODOS.rejected: {
        const error = {
          api: action.payload.response.data,
        };

        newState = {
          ...state,
          todoApiErrors: error,
          hasActiveGetTodosAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_TODO.base: {
        newState = {
          ...state,
          hasActiveCreateTodoAction: true,
        };
        break;
      }


      case ACTIONS.CREATE_TODO.fulfilled: {
        const todo = action.payload.response.data.task;
        const { todos } = state;

        newState = {
          ...state,
          todos: [
            ...todos,
            todo,
          ],
          todoApiErrors: {},
          hasActiveCreateTodoAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_TODO.rejected: {
        const error = {
          api: action.payload.response.data,
        };

        newState = {
          ...state,
          todoApiErrors: error,
          hasActiveCreateTodoAction: false,
        };

        break;
      }

      case ACTIONS.UPDATE_TODO.base: {
        newState = {
          ...state,
          hasActiveUpdateTodoAction: true,
        };
        break;
      }


      case ACTIONS.UPDATE_TODO.fulfilled: {
        const todo = action.payload.response.data.task;
        const { todos } = state;
        const todoIndex = todos.findIndex(item => item.id === todo.id);

        newState = {
          ...state,
          todos: [
            ...todos.slice(0, todoIndex),
            {
              ...todo,
            },
            ...todos.slice(todoIndex + 1),
          ],
          todoApiErrors: {},
          hasActiveUpdateTodoAction: false,
        };

        break;
      }

      case ACTIONS.UPDATE_TODO.rejected: {
        const error = {
          api: action.payload.response.data,
        };

        newState = {
          ...state,
          todoApiErrors: error,
          hasActiveUpdateTodoAction: false,
        };

        break;
      }

      case ACTIONS.DELETE_TODO.base: {
        const { id } = action.payload;

        newState = {
          ...state,
          activeDeleteTodoActionCandidateId: id,
          hasActiveDeleteTodoAction: true,
        };
        break;
      }


      case ACTIONS.DELETE_TODO.fulfilled: {
        const { todos, activeDeleteTodoActionCandidateId } = state;
        const todoIndex = todos.findIndex(item => item.id === activeDeleteTodoActionCandidateId);

        newState = {
          ...state,
          todos: [
            ...todos.slice(0, todoIndex),
            ...todos.slice(todoIndex + 1),
          ],
          activeDeleteTodoActionCandidateId: "",
          hasActiveDeleteTodoAction: false,
        };

        break;
      }

      case ACTIONS.DELETE_TODO.rejected: {
        const error = {
          api: action.payload.response.data,
        };

        newState = {
          ...state,
          todoApiErrors: error,
          activeDeleteTodoActionCandidateId: "",
          hasActiveDeleteTodoAction: false,
        };

        break;
      }


      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default TodoReducer;
