import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getTodosFulfilled,
  getTodosRejected,
  createTodoFulfilled,
  createTodoRejected,
  updateTodoFulfilled,
  updateTodoRejected,
  deleteTodoFulfilled,
  deleteTodoRejected,
} from './todoActions';
import todoApi from './todoApi';

function* getTodos() {
  try {
    const todos = yield call(todoApi.getTodos);
    yield put(getTodosFulfilled(todos));
  } catch (e) {
    yield put(getTodosRejected(e));
  }
}

function* createTodo(action) {
  try {
    const response = yield call(todoApi.createTodo, action.payload.todo);
    yield put(createTodoFulfilled(response));
  } catch (e) {
    yield put(createTodoRejected(e));
  }
}

function* updateTodo(action) {
  try {
    const response = yield call(todoApi.updateTodo, action.payload.todo);
    yield put(updateTodoFulfilled(response));
  } catch (e) {
    yield put(updateTodoRejected(e));
  }
}

function* deleteTodo(action) {
  try {
    const response = yield call(todoApi.deleteTodo, action.payload.id);
    yield put(deleteTodoFulfilled(response));
  } catch (e) {
    yield put(deleteTodoRejected(e));
  }
}

const logSagas = [
  takeEvery(ACTIONS.GET_TODOS.base, getTodos),
  takeEvery(ACTIONS.CREATE_TODO.base, createTodo),
  takeEvery(ACTIONS.UPDATE_TODO.base, updateTodo),
  takeEvery(ACTIONS.DELETE_TODO.base, deleteTodo),
];

export default logSagas;
