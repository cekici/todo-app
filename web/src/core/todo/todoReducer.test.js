import * as ACTIONS from '../../constants/actionTypes.js';
import TodoReducer from './TodoReducer.js';

const initialState = {
  todos: [],
  todoApiErrors: {},
  hasActiveGetTodosAction: false,
  hasActiveCreateTodoAction: false,
  hasActiveUpdateTodoAction: false,
  hasActiveDeleteTodoAction: false,
  activeDeleteTodoActionCandidateId: ""
};

describe('TodoReducer', () => {
  const reducer = new TodoReducer();

  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
      .toEqual(initialState);
  });
});
