import axios from 'axios';

const baseConfig = {
  baseURL: 'http://localhost:9001',
};

const networkManager = {
  init() {
    this.api = axios.create(baseConfig);
  },
};

networkManager.init();
export default networkManager;
