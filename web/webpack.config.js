const path = require("path");
const postCssConfig = require("./postcss.config.js");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanPlugin = require("clean-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const basePath = path.resolve(__dirname, "");
const distPath = basePath + "/dist";
const srcPath = basePath + "/src";

module.exports = {
  entry: {
    "sl-app": srcPath + "/index.jsx"
  },

  output: {
    publicPath: "/",
    path: distPath,
    filename: "[name].js",
  },

  devtool: "source-map",

  module: {
    rules: [
      {
        test: /.(woff(2)?|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        exclude: /node_modules/,
        loader: "url-loader"
      },
      {
        test: /\.scss/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract({
          fallbackLoader: "style-loader",
          loader: [
            {
              loader: "css-loader",
              options: {
                importLoaders: 1
              }
            },
            {
              loader: "postcss-loader",
              options: postCssConfig
            },
            {
              loader: "sass-loader"
            }
          ]
        })
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          cacheDirectory: true
        }
      }]
  },

  resolve: {
    extensions: [".js", ".jsx"],
    modules: [
      "node_modules"
    ]
  },

  plugins: [
    new CleanPlugin([distPath], {
      verbose: true,
      dry: false
    }),
    new ExtractTextPlugin({
      filename: "[name].css",
      disable: false,
      allChunks: true
    }),
    new WriteFilePlugin(),
    new CopyWebpackPlugin([

      {
        from: srcPath + "/manifest.json",
        to: distPath
      }
    ]),
    new HtmlWebpackPlugin({
      template: `${srcPath}/index.html`,
      filename: "index.html",
      chunks: ["sl-app"]
    }),
    new SWPrecacheWebpackPlugin({
      cacheId: 'todo-app',
      dontCacheBustUrlsMatching: /\.\w{8}\./,
      filename: 'service-worker.js',
      minify: true,
      navigateFallback: distPath + 'index.html',
      staticFileGlobs: [
        'dist/index.html',
        'dist/manifest.json',
        'dist/**/!(*map*)'
      ],    }),
  ]
};