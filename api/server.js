'use strict';

const app = require('express')();
const tasksContainer = require('./tasks.json');
const bodyParser = require('body-parser');
const shortid = require('shortid');

app.use(bodyParser.json());

app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');

  if (req.method === 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

/**
 * GET /tasks
 *
 * Return the list of tasks with status code 200.
 */
app.get('/tasks', (req, res) => {
  return res.status(200).json(tasksContainer);
});

/**
 * Get /task/:id
 *
 * id: Number
 *
 * Return the task for the given id.
 *
 * If found return status code 200 and the resource.
 * If not found return status code 404.
 * If id is not valid number return status code 400.
 */
app.get('/task/:id', (req, res) => {
  const task = tasksContainer.find((item) => item.id === req.params.id);

  if (task !== null) {
    return res.status(200).json({
      task,
    });
  } else {
    return res.status(404).json({
      message: 'Not found.',
    });
  }
})
;


/**
 * POST /task/create
 *
 * title: string
 * description: string
 *
 * Add a new task to the array tasksContainer.tasks with the given title and description.
 * Return status code 201.
 */
app.post('/task', (req, res) => {
  const task = {
    id: shortid.generate(),
    title: req.body.title,
    description: req.body.description,
    isCompleted: false
  };

  tasksContainer.tasks.push(task);

  return res.status(201).json({
    task: task,
    message: 'Resource created',
  });
});

/**
 * PUT /task/update/:id
 *
 * id: Number
 * title: string
 * description: string
 * isCompleted: bool
 *
 * Update the task with the given id.
 * If the task is found and update as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not a valid number return a status code 400.
 */
app.put('/task/:id', (req, res) => {
  const task = tasksContainer.tasks.find(item => item.id === req.params.id);

  if (task != null) {
    task.title = req.body.title;
    task.description = req.body.description;
    task.isCompleted = req.body.isCompleted;
    return res.status(200).json({
      task: task,
      message: 'Resource updated'
    });
  } else {
    return res.status(404).json({
      message: 'Not found',
    });
  }
});

/**
 * DELETE /task/:id
 *
 * id: Number
 *
 * Delete the task linked to the  given id.
 * If the task is found and deleted as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not a valid number return a status code 400.
 */
app.delete('/task/:id', (req, res) => {
  const task = tasksContainer.tasks.find(item => item.id === req.params.id);

  if (task !== null) {
    const taskIndex = tasksContainer.tasks;
    tasksContainer.tasks.splice(taskIndex, 1);
    return res.status(200).json({
      message: 'Updated successfully',
    });
  } else {
    return es.status(404).json({
      message: 'Not found',
    });
  }
})
;

app.listen(9001, () => {
  process.stdout.write('the server is available on http://localhost:9001/\n');
});
